<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* img/comilla.svg */
class __TwigTemplate_3fe6e4901566ad1bed0e01d988e40d691deb5c5c61fc913f8b07be9206c44d25 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/comilla.svg"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/comilla.svg"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
\t viewBox=\"0 0 52.5 48\" style=\"enable-background:new 0 0 52.5 48;\" xml:space=\"preserve\">
<style type=\"text/css\">
\t.st0{enable-background:new    ;}
\t.st1{fill:#FFFFFF;}
</style>
<g class=\"st0\">
\t<path class=\"st1\" d=\"M9.7,40.8c-2.7-4.3-4-8.7-4-13.1c0-3.9,1-7.6,3-11.2c2-3.6,4.8-6.8,8.6-9.8l4.5,2.7c0.4,0.2,0.7,0.5,0.8,0.8
\t\tc0.2,0.3,0.2,0.7,0.2,1c0,0.4-0.1,0.7-0.2,1c-0.2,0.3-0.3,0.6-0.5,0.8c-0.5,0.6-1,1.3-1.6,2c-0.6,0.8-1.1,1.6-1.6,2.5
\t\tc-0.5,0.9-0.9,1.9-1.2,3c-0.3,1.1-0.5,2.3-0.5,3.5c0,1.3,0.2,2.7,0.6,4.2c0.4,1.5,1.1,3.1,2.2,4.7c0.4,0.5,0.5,1.1,0.5,1.6
\t\tc0,1.2-0.7,2.1-2.1,2.6L9.7,40.8z M29.1,40.8c-2.7-4.3-4-8.7-4-13.1c0-3.9,1-7.6,3-11.2c2-3.6,4.8-6.8,8.6-9.8l4.5,2.7
\t\tc0.4,0.2,0.7,0.5,0.8,0.8c0.2,0.3,0.2,0.7,0.2,1c0,0.4-0.1,0.7-0.2,1c-0.2,0.3-0.3,0.6-0.5,0.8c-0.5,0.6-1,1.3-1.6,2
\t\tc-0.6,0.8-1.1,1.6-1.5,2.5c-0.5,0.9-0.9,1.9-1.2,3c-0.3,1.1-0.5,2.3-0.5,3.5c0,1.3,0.2,2.7,0.6,4.2c0.4,1.5,1.1,3.1,2.2,4.7
\t\tc0.4,0.5,0.5,1.1,0.5,1.6c0,1.2-0.7,2.1-2.1,2.6L29.1,40.8z\"/>
</g>
</svg>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "img/comilla.svg";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
\t viewBox=\"0 0 52.5 48\" style=\"enable-background:new 0 0 52.5 48;\" xml:space=\"preserve\">
<style type=\"text/css\">
\t.st0{enable-background:new    ;}
\t.st1{fill:#FFFFFF;}
</style>
<g class=\"st0\">
\t<path class=\"st1\" d=\"M9.7,40.8c-2.7-4.3-4-8.7-4-13.1c0-3.9,1-7.6,3-11.2c2-3.6,4.8-6.8,8.6-9.8l4.5,2.7c0.4,0.2,0.7,0.5,0.8,0.8
\t\tc0.2,0.3,0.2,0.7,0.2,1c0,0.4-0.1,0.7-0.2,1c-0.2,0.3-0.3,0.6-0.5,0.8c-0.5,0.6-1,1.3-1.6,2c-0.6,0.8-1.1,1.6-1.6,2.5
\t\tc-0.5,0.9-0.9,1.9-1.2,3c-0.3,1.1-0.5,2.3-0.5,3.5c0,1.3,0.2,2.7,0.6,4.2c0.4,1.5,1.1,3.1,2.2,4.7c0.4,0.5,0.5,1.1,0.5,1.6
\t\tc0,1.2-0.7,2.1-2.1,2.6L9.7,40.8z M29.1,40.8c-2.7-4.3-4-8.7-4-13.1c0-3.9,1-7.6,3-11.2c2-3.6,4.8-6.8,8.6-9.8l4.5,2.7
\t\tc0.4,0.2,0.7,0.5,0.8,0.8c0.2,0.3,0.2,0.7,0.2,1c0,0.4-0.1,0.7-0.2,1c-0.2,0.3-0.3,0.6-0.5,0.8c-0.5,0.6-1,1.3-1.6,2
\t\tc-0.6,0.8-1.1,1.6-1.5,2.5c-0.5,0.9-0.9,1.9-1.2,3c-0.3,1.1-0.5,2.3-0.5,3.5c0,1.3,0.2,2.7,0.6,4.2c0.4,1.5,1.1,3.1,2.2,4.7
\t\tc0.4,0.5,0.5,1.1,0.5,1.6c0,1.2-0.7,2.1-2.1,2.6L29.1,40.8z\"/>
</g>
</svg>
", "img/comilla.svg", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\img\\comilla.svg");
    }
}
