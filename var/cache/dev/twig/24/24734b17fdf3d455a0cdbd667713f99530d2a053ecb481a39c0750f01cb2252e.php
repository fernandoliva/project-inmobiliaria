<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* img/barras.svg */
class __TwigTemplate_93a0b0edca488f5f00f3cd1dc448e0783a422b247665ce50d7c514dbd7859c0f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/barras.svg"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/barras.svg"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">
<svg version=\"1.1\" id=\"Capa_1\" focusable=\"false\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"
\t x=\"0px\" y=\"0px\" width=\"612px\" height=\"792px\" viewBox=\"0 0 612 792\" enable-background=\"new 0 0 612 792\" xml:space=\"preserve\">
<path fill=\"#FFFFFF\" d=\"M21.857,226.607h568.286c12.072,0,21.857-9.785,21.857-21.857v-54.643c0-12.072-9.785-21.857-21.857-21.857
\tH21.857C9.785,128.25,0,138.035,0,150.107v54.643C0,216.822,9.785,226.607,21.857,226.607z M21.857,445.179h568.286
\tc12.072,0,21.857-9.785,21.857-21.857v-54.643c0-12.072-9.785-21.857-21.857-21.857H21.857C9.785,346.821,0,356.606,0,368.679
\tv54.643C0,435.394,9.785,445.179,21.857,445.179z M21.857,663.75h568.286c12.072,0,21.857-9.785,21.857-21.857V587.25
\tc0-12.072-9.785-21.857-21.857-21.857H21.857C9.785,565.393,0,575.178,0,587.25v54.643C0,653.965,9.785,663.75,21.857,663.75z\"/>
</svg>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "img/barras.svg";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">
<svg version=\"1.1\" id=\"Capa_1\" focusable=\"false\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"
\t x=\"0px\" y=\"0px\" width=\"612px\" height=\"792px\" viewBox=\"0 0 612 792\" enable-background=\"new 0 0 612 792\" xml:space=\"preserve\">
<path fill=\"#FFFFFF\" d=\"M21.857,226.607h568.286c12.072,0,21.857-9.785,21.857-21.857v-54.643c0-12.072-9.785-21.857-21.857-21.857
\tH21.857C9.785,128.25,0,138.035,0,150.107v54.643C0,216.822,9.785,226.607,21.857,226.607z M21.857,445.179h568.286
\tc12.072,0,21.857-9.785,21.857-21.857v-54.643c0-12.072-9.785-21.857-21.857-21.857H21.857C9.785,346.821,0,356.606,0,368.679
\tv54.643C0,435.394,9.785,445.179,21.857,445.179z M21.857,663.75h568.286c12.072,0,21.857-9.785,21.857-21.857V587.25
\tc0-12.072-9.785-21.857-21.857-21.857H21.857C9.785,565.393,0,575.178,0,587.25v54.643C0,653.965,9.785,663.75,21.857,663.75z\"/>
</svg>
", "img/barras.svg", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\img\\barras.svg");
    }
}
