<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* img/logoFinal.svg */
class __TwigTemplate_38dfc3c6c9c7d84db47852b8d38a0174c41a7a617ceb3b35779177ec14019455 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/logoFinal.svg"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/logoFinal.svg"));

        // line 1
        echo "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"1242\" height=\"144\" viewBox=\"0 0 1242 144\">
  <defs>
    <style>
      .cls-1 {
        font-size: 200px;
        text-anchor: middle;
        fill: #fff;
        font-family: \"Gill Sans\";
        font-weight: 300;
      }

      .cls-2 {
        font-weight: 500;
      }
    </style>
  </defs>
  <text id=\"INMOFOLIVA\" class=\"cls-1\" x=\"4\" y=\"7\"><tspan x=\"4\">INMO<tspan class=\"cls-2\">FOLIVA</tspan></tspan></text>
</svg>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "img/logoFinal.svg";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"1242\" height=\"144\" viewBox=\"0 0 1242 144\">
  <defs>
    <style>
      .cls-1 {
        font-size: 200px;
        text-anchor: middle;
        fill: #fff;
        font-family: \"Gill Sans\";
        font-weight: 300;
      }

      .cls-2 {
        font-weight: 500;
      }
    </style>
  </defs>
  <text id=\"INMOFOLIVA\" class=\"cls-1\" x=\"4\" y=\"7\"><tspan x=\"4\">INMO<tspan class=\"cls-2\">FOLIVA</tspan></tspan></text>
</svg>
", "img/logoFinal.svg", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\img\\logoFinal.svg");
    }
}
