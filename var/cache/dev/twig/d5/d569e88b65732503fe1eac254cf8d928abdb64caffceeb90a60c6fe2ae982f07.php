<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* propiedades/index.html.twig */
class __TwigTemplate_ef5b6de7edc4622fa85489ace2d9768542e55aba8be50a451fe65bd1fd0f6fa9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "propiedades/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "propiedades/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "propiedades/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Propiedades";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1>Propiedades</h1>

    ";
        // line 47
        echo "

    <main class=\"contenedor seccion\">
    <a href=\"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("propiedades_new");
        echo "\" class=\"boton boton-verde\">Nueva Propiedad</a>

        <table class=\"propiedades\">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Precio</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody> <!--Mostrar los resultados-->
            ";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["propiedades"]) || array_key_exists("propiedades", $context) ? $context["propiedades"] : (function () { throw new RuntimeError('Variable "propiedades" does not exist.', 63, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["propiedade"]) {
            // line 64
            echo "                <tr>
                    <td>";
            // line 65
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "id", [], "any", false, false, false, 65), "html", null, true);
            echo "</td>
                    <td>";
            // line 66
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "nombre", [], "any", false, false, false, 66), "html", null, true);
            echo "</td>
                    <td><img src=\"/build/img/";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "imagen", [], "any", false, false, false, 67), "html", null, true);
            echo "\" alt=\"\" class=\"imagen-tabla\"></td>
                    <td>";
            // line 68
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "precio", [], "any", false, false, false, 68), "html", null, true);
            echo "€</td>
                    <td>
                        <a href=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("propiedades_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["propiedade"], "id", [], "any", false, false, false, 70)]), "html", null, true);
            echo "\" class=\"boton-amarillo-block\">Actualizar</a>
                        <form method=\"post\" action=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("propiedades_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["propiedade"], "id", [], "any", false, false, false, 71)]), "html", null, true);
            echo "\" onsubmit=\"return confirm('¿Estás seguro de eliminar esta propiedad?');\" class=\"w-100\">
                            <input type=\"hidden\" name=\"_token\" value=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("delete" . twig_get_attribute($this->env, $this->source, $context["propiedade"], "id", [], "any", false, false, false, 72))), "html", null, true);
            echo "\">
                            <button  class=\"boton-rojo-block\">Eliminar</button>
                        </form>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['propiedade'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "            </tbody>
        </table>
        </main>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "propiedades/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 78,  145 => 72,  141 => 71,  137 => 70,  132 => 68,  128 => 67,  124 => 66,  120 => 65,  117 => 64,  113 => 63,  97 => 50,  92 => 47,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Propiedades{% endblock %}

{% block body %}
    <h1>Propiedades</h1>

    {# <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Imagen</th>
                <th>Descripcion</th>
                <th>Habitaciones</th>
                <th>Wc</th>
                <th>Parking</th>
                <th>Creado</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
        {% for propiedade in propiedades %}
            <tr>
                <td>{{ propiedade.id }}</td>
                <td>{{ propiedade.nombre }}</td>
                <td>{{ propiedade.precio }}</td>
                <td>{{ propiedade.imagen }}</td>
                <td>{{ propiedade.descripcion }}</td>
                <td>{{ propiedade.habitaciones }}</td>
                <td>{{ propiedade.wc }}</td>
                <td>{{ propiedade.parking }}</td>
                <td>{{ propiedade.creado ? propiedade.creado|date('Y-m-d') : '' }}</td>
                <td>
                    <a href=\"{{ path('propiedades_show', {'id': propiedade.id}) }}\">show</a>
                    <a href=\"{{ path('propiedades_edit', {'id': propiedade.id}) }}\">edit</a>
                </td>
            </tr>
        {% else %}
            <tr>
                <td colspan=\"10\">no records found</td>
            </tr>
        {% endfor %}
        </tbody>
    </table> #}


    <main class=\"contenedor seccion\">
    <a href=\"{{ path('propiedades_new') }}\" class=\"boton boton-verde\">Nueva Propiedad</a>

        <table class=\"propiedades\">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Precio</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody> <!--Mostrar los resultados-->
            {% for propiedade in propiedades %}
                <tr>
                    <td>{{ propiedade.id }}</td>
                    <td>{{ propiedade.nombre }}</td>
                    <td><img src=\"/build/img/{{ propiedade.imagen }}\" alt=\"\" class=\"imagen-tabla\"></td>
                    <td>{{ propiedade.precio }}€</td>
                    <td>
                        <a href=\"{{ path('propiedades_edit', {'id': propiedade.id}) }}\" class=\"boton-amarillo-block\">Actualizar</a>
                        <form method=\"post\" action=\"{{ path('propiedades_delete', {'id': propiedade.id}) }}\" onsubmit=\"return confirm('¿Estás seguro de eliminar esta propiedad?');\" class=\"w-100\">
                            <input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('delete' ~ propiedade.id) }}\">
                            <button  class=\"boton-rojo-block\">Eliminar</button>
                        </form>
                    </td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
        </main>
{% endblock %}
", "propiedades/index.html.twig", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\propiedades\\index.html.twig");
    }
}
