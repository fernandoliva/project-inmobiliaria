<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact/contact.html.twig */
class __TwigTemplate_66bcc3f4c5f068f13b8a042d0c1ac354240c5fc8fa32c90e775d965872deb551 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact/contact.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact/contact.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contact/contact.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"contenedor seccion\">
        <h1>Contacto</h1>
        <picture>
            <source srcset=\"build/img/destacada3.webp\" type=\"image/webp\">
            <source srcset=\"build/img/destacada3.jpg\" type=\"image/jpeg\">
            <img src=\"build/img/destacada3.jpg\" alt=\"Imagen contacto\" loading=\"lazy\">
        </picture>

        <h2>Rellena el formulario de contacto</h2>

        <form action=\"\" class=\"formulario\">
            <fieldset>
                <legend>Información Personal</legend>
                <label for=\"nombre\">Nombre</label>
                <input type=\"text\" placeholder=\"Escribe tu nombre\" id=\"nombre\">
                <label for=\"email\">E-mail</label>
                <input type=\"email\" placeholder=\"Escribe tu correo electrónico\" id=\"email\">
                <label for=\"tel\">Email</label>
                <input type=\"tel\" placeholder=\"Escribe tu teléfono\" id=\"tel\">
                <label for=\"mensaje\">Mensaje:</label>
                <textarea id=\"mensaje\" placeholder=\"Escribe tu mensaje\"></textarea>
            </fieldset>
            <fieldset>
                <legend>Información sobre la propiedad</legend>
                <label for=\"opciones\">Tipo:</label>
                <select id=\"opciones\">
                    <option value=\"\" disabled selected>-- Seleccionar --</option>
                    <option value=\"Compra\">Compra</option>
                    <option value=\"Vende\">Vende</option>
                </select>
                <label for=\"presupuesto\">Presupuesto</label>
                <input type=\"number\" placeholder=\"Tu presupuesto\" id=\"presupuesto\">
            </fieldset>
            <fieldset>
                <legend>Contacto</legend>
                <p>¿Como quieres que te contactemos?</p>
                <div class=\"forma-contacto\">
                    <label for=\"contactar-telefono\">Teléfono</label>
                    <input name=\"contacto\" type=\"radio\" value=\"Telefono\" id=\"contactar-telefono\">
                    <label for=\"contactar-email\">E-mail</label>
                    <input name=\"contacto\" type=\"radio\" value=\"E-mail\" id=\"contactar-email\">
                </div>
                <p>Elige tu Fecha/Hora</p>
                <label for=\"fecha\">Fecha</label>
                <input type=\"date\" id=\"fecha\">
                <label for=\"hora\">Hora</label>
                <input type=\"time\" id=\"hora\" min=\"09:00\" max=\"18:00\">
            </fieldset>
            <input type=\"submit\" value=\"Enviar\" class=\"boton-verde\">
        </form>
    </main>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contact/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block body %}
    <main class=\"contenedor seccion\">
        <h1>Contacto</h1>
        <picture>
            <source srcset=\"build/img/destacada3.webp\" type=\"image/webp\">
            <source srcset=\"build/img/destacada3.jpg\" type=\"image/jpeg\">
            <img src=\"build/img/destacada3.jpg\" alt=\"Imagen contacto\" loading=\"lazy\">
        </picture>

        <h2>Rellena el formulario de contacto</h2>

        <form action=\"\" class=\"formulario\">
            <fieldset>
                <legend>Información Personal</legend>
                <label for=\"nombre\">Nombre</label>
                <input type=\"text\" placeholder=\"Escribe tu nombre\" id=\"nombre\">
                <label for=\"email\">E-mail</label>
                <input type=\"email\" placeholder=\"Escribe tu correo electrónico\" id=\"email\">
                <label for=\"tel\">Email</label>
                <input type=\"tel\" placeholder=\"Escribe tu teléfono\" id=\"tel\">
                <label for=\"mensaje\">Mensaje:</label>
                <textarea id=\"mensaje\" placeholder=\"Escribe tu mensaje\"></textarea>
            </fieldset>
            <fieldset>
                <legend>Información sobre la propiedad</legend>
                <label for=\"opciones\">Tipo:</label>
                <select id=\"opciones\">
                    <option value=\"\" disabled selected>-- Seleccionar --</option>
                    <option value=\"Compra\">Compra</option>
                    <option value=\"Vende\">Vende</option>
                </select>
                <label for=\"presupuesto\">Presupuesto</label>
                <input type=\"number\" placeholder=\"Tu presupuesto\" id=\"presupuesto\">
            </fieldset>
            <fieldset>
                <legend>Contacto</legend>
                <p>¿Como quieres que te contactemos?</p>
                <div class=\"forma-contacto\">
                    <label for=\"contactar-telefono\">Teléfono</label>
                    <input name=\"contacto\" type=\"radio\" value=\"Telefono\" id=\"contactar-telefono\">
                    <label for=\"contactar-email\">E-mail</label>
                    <input name=\"contacto\" type=\"radio\" value=\"E-mail\" id=\"contactar-email\">
                </div>
                <p>Elige tu Fecha/Hora</p>
                <label for=\"fecha\">Fecha</label>
                <input type=\"date\" id=\"fecha\">
                <label for=\"hora\">Hora</label>
                <input type=\"time\" id=\"hora\" min=\"09:00\" max=\"18:00\">
            </fieldset>
            <input type=\"submit\" value=\"Enviar\" class=\"boton-verde\">
        </form>
    </main>


{% endblock %}", "contact/contact.html.twig", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\contact\\contact.html.twig");
    }
}
