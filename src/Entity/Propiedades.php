<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Propiedades
 *
 * @ORM\Table(name="propiedades", indexes={@ORM\Index(name="vendedorID_idx", columns={"vendedorId"})})
 * @ORM\Entity
 */
class Propiedades
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=60, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $precio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen", type="string", length=200, nullable=true)
     */
    private $imagen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="text", length=0, nullable=true)
     */
    private $descripcion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="habitaciones", type="integer", nullable=true)
     */
    private $habitaciones;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wc", type="integer", nullable=true)
     */
    private $wc;

    /**
     * @var int|null
     *
     * @ORM\Column(name="parking", type="integer", nullable=true)
     */
    private $parking;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creado", type="date", nullable=true)
     */
    private $creado;

    /**
     * @var \Vendedores
     *
     * @ORM\ManyToOne(targetEntity="Vendedores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vendedorId", referencedColumnName="id")
     * })
     */
    private $vendedorid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getPrecio(): ?string
    {
        return $this->precio;
    }

    public function setPrecio(?string $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(?string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getHabitaciones(): ?int
    {
        return $this->habitaciones;
    }

    public function setHabitaciones(?int $habitaciones): self
    {
        $this->habitaciones = $habitaciones;

        return $this;
    }

    public function getWc(): ?int
    {
        return $this->wc;
    }

    public function setWc(?int $wc): self
    {
        $this->wc = $wc;

        return $this;
    }

    public function getParking(): ?int
    {
        return $this->parking;
    }

    public function setParking(?int $parking): self
    {
        $this->parking = $parking;

        return $this;
    }

    public function getCreado(): ?\DateTimeInterface
    {
        return $this->creado;
    }

    public function setCreado(?\DateTimeInterface $creado): self
    {
        $this->creado = $creado;

        return $this;
    }

    // public function getVendedorid(): ?Vendedores
    // {
    //     return $this->vendedorid;
    // }

    // public function setVendedorid(?Vendedores $vendedorid): self
    // {
    //     $this->vendedorid = $vendedorid;

    //     return $this;
    // }

    private $image;

    public function getImage()
    {
        return $this->image;
    }
    
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }


}
