<?php

namespace Container9FWQfqx;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHoldere877d = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer48842 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesc22e9 = [
        
    ];

    public function getConnection()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getConnection', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getMetadataFactory', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getExpressionBuilder', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'beginTransaction', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getCache', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getCache();
    }

    public function transactional($func)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'transactional', array('func' => $func), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'wrapInTransaction', array('func' => $func), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'commit', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->commit();
    }

    public function rollback()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'rollback', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getClassMetadata', array('className' => $className), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'createQuery', array('dql' => $dql), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'createNamedQuery', array('name' => $name), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'createQueryBuilder', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'flush', array('entity' => $entity), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'clear', array('entityName' => $entityName), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->clear($entityName);
    }

    public function close()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'close', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->close();
    }

    public function persist($entity)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'persist', array('entity' => $entity), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'remove', array('entity' => $entity), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'refresh', array('entity' => $entity), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'detach', array('entity' => $entity), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'merge', array('entity' => $entity), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getRepository', array('entityName' => $entityName), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'contains', array('entity' => $entity), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getEventManager', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getConfiguration', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'isOpen', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getUnitOfWork', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getProxyFactory', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'initializeObject', array('obj' => $obj), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'getFilters', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'isFiltersStateClean', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'hasFilters', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return $this->valueHoldere877d->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer48842 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHoldere877d) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHoldere877d = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHoldere877d->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, '__get', ['name' => $name], $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        if (isset(self::$publicPropertiesc22e9[$name])) {
            return $this->valueHoldere877d->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere877d;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldere877d;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere877d;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldere877d;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, '__isset', array('name' => $name), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere877d;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldere877d;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, '__unset', array('name' => $name), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere877d;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldere877d;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, '__clone', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        $this->valueHoldere877d = clone $this->valueHoldere877d;
    }

    public function __sleep()
    {
        $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, '__sleep', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;

        return array('valueHoldere877d');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer48842 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer48842;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer48842 && ($this->initializer48842->__invoke($valueHoldere877d, $this, 'initializeProxy', array(), $this->initializer48842) || 1) && $this->valueHoldere877d = $valueHoldere877d;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldere877d;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHoldere877d;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
