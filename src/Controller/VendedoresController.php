<?php

namespace App\Controller;

use App\Entity\Vendedores;
use App\Form\VendedoresType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/vendedores')]
class VendedoresController extends AbstractController
{
    #[Route('/', name: 'vendedores_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $vendedores = $entityManager
            ->getRepository(Vendedores::class)
            ->findAll();

        return $this->render('vendedores/index.html.twig', [
            'vendedores' => $vendedores,
        ]);
    }

    #[Route('/new', name: 'vendedores_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $vendedore = new Vendedores();
        $form = $this->createForm(VendedoresType::class, $vendedore);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($vendedore);
            $entityManager->flush();

            return $this->redirectToRoute('vendedores_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('vendedores/new.html.twig', [
            'vendedore' => $vendedore,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'vendedores_show', methods: ['GET'])]
    public function show(Vendedores $vendedore): Response
    {
        return $this->render('vendedores/show.html.twig', [
            'vendedore' => $vendedore,
        ]);
    }

    #[Route('/{id}/edit', name: 'vendedores_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Vendedores $vendedore, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(VendedoresType::class, $vendedore);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('vendedores_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('vendedores/edit.html.twig', [
            'vendedore' => $vendedore,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'vendedores_delete', methods: ['POST'])]
    public function delete(Request $request, Vendedores $vendedore, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$vendedore->getId(), $request->request->get('_token'))) {
            $entityManager->remove($vendedore);
            $entityManager->flush();
        }

        return $this->redirectToRoute('vendedores_index', [], Response::HTTP_SEE_OTHER);
    }
}
