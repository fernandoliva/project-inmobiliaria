<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* img/icono3.svg */
class __TwigTemplate_26dc425c7cb041685224eb686c2f0a1aa32f892a192098f2f03627a60cc58be3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/icono3.svg"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/icono3.svg"));

        // line 1
        echo "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"93\" height=\"93\" viewBox=\"0 0 93 93\">
  <g id=\"icono3\" transform=\"translate(-11.625)\">
    <g id=\"Group_11\" data-name=\"Group 11\">
      <g id=\"Group_10\" data-name=\"Group 10\">
        <path id=\"Path_7\" data-name=\"Path 7\" d=\"M58.125,0a46.5,46.5,0,1,0,46.5,46.5A46.5,46.5,0,0,0,58.125,0ZM60.12,88.911V80.732a2,2,0,1,0-3.99,0v8.179A42.49,42.49,0,0,1,15.714,48.5h8.179a1.995,1.995,0,1,0,0-3.99H15.714A42.49,42.49,0,0,1,56.13,4.089v8.179a1.995,1.995,0,1,0,3.99,0V4.089a42.49,42.49,0,0,1,40.416,40.416H92.357a2,2,0,1,0,0,3.99h8.2A42.49,42.49,0,0,1,60.12,88.911Z\" fill=\"#e08709\"/>
      </g>
    </g>
    <g id=\"Group_13\" data-name=\"Group 13\">
      <g id=\"Group_12\" data-name=\"Group 12\">
        <path id=\"Path_8\" data-name=\"Path 8\" d=\"M79.171,65.052,60.12,45.682V25.554a2,2,0,1,0-3.99,0V46.5a2,2,0,0,0,.578,1.4l19.61,19.945a2,2,0,0,0,2.853-2.793Z\" fill=\"#e08709\"/>
      </g>
    </g>
  </g>
</svg>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "img/icono3.svg";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"93\" height=\"93\" viewBox=\"0 0 93 93\">
  <g id=\"icono3\" transform=\"translate(-11.625)\">
    <g id=\"Group_11\" data-name=\"Group 11\">
      <g id=\"Group_10\" data-name=\"Group 10\">
        <path id=\"Path_7\" data-name=\"Path 7\" d=\"M58.125,0a46.5,46.5,0,1,0,46.5,46.5A46.5,46.5,0,0,0,58.125,0ZM60.12,88.911V80.732a2,2,0,1,0-3.99,0v8.179A42.49,42.49,0,0,1,15.714,48.5h8.179a1.995,1.995,0,1,0,0-3.99H15.714A42.49,42.49,0,0,1,56.13,4.089v8.179a1.995,1.995,0,1,0,3.99,0V4.089a42.49,42.49,0,0,1,40.416,40.416H92.357a2,2,0,1,0,0,3.99h8.2A42.49,42.49,0,0,1,60.12,88.911Z\" fill=\"#e08709\"/>
      </g>
    </g>
    <g id=\"Group_13\" data-name=\"Group 13\">
      <g id=\"Group_12\" data-name=\"Group 12\">
        <path id=\"Path_8\" data-name=\"Path 8\" d=\"M79.171,65.052,60.12,45.682V25.554a2,2,0,1,0-3.99,0V46.5a2,2,0,0,0,.578,1.4l19.61,19.945a2,2,0,0,0,2.853-2.793Z\" fill=\"#e08709\"/>
      </g>
    </g>
  </g>
</svg>
", "img/icono3.svg", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\img\\icono3.svg");
    }
}
