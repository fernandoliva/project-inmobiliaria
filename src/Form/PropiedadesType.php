<?php

namespace App\Form;

use App\Entity\Propiedades;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropiedadesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre')
            ->add('precio')
            ->add('imagen',FileType::class,array('data_class'=> null, 'label' => 'Image', 'required' => false))
            ->add('descripcion')
            ->add('habitaciones')
            ->add('wc')
            ->add('parking')
            ->add('creado')

            // ->add('vendedorid')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Propiedades::class,
        ]);
    }
}
