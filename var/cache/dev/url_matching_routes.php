<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/contact' => [[['_route' => 'contact', '_controller' => 'App\\Controller\\ContactController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'homePage', '_controller' => 'App\\Controller\\HomeController::index'], null, ['GET' => 0], null, false, false, null]],
        '/propiedades' => [[['_route' => 'propiedades_index', '_controller' => 'App\\Controller\\PropiedadesController::index'], null, ['GET' => 0], null, true, false, null]],
        '/propiedades/new' => [[['_route' => 'propiedades_new', '_controller' => 'App\\Controller\\PropiedadesController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/usuarios' => [[['_route' => 'usuarios_index', '_controller' => 'App\\Controller\\UsuariosController::index'], null, ['GET' => 0], null, true, false, null]],
        '/usuarios/new' => [[['_route' => 'usuarios_new', '_controller' => 'App\\Controller\\UsuariosController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/vendedores' => [[['_route' => 'vendedores_index', '_controller' => 'App\\Controller\\VendedoresController::index'], null, ['GET' => 0], null, true, false, null]],
        '/vendedores/new' => [[['_route' => 'vendedores_new', '_controller' => 'App\\Controller\\VendedoresController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|wdt/([^/]++)(*:24)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:69)'
                            .'|router(*:82)'
                            .'|exception(?'
                                .'|(*:101)'
                                .'|\\.css(*:114)'
                            .')'
                        .')'
                        .'|(*:124)'
                    .')'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:159)'
                .')'
                .'|/propiedades/([^/]++)(?'
                    .'|(*:192)'
                    .'|/edit(*:205)'
                    .'|(*:213)'
                .')'
                .'|/usuarios/([^/]++)(?'
                    .'|(*:243)'
                    .'|/edit(*:256)'
                    .'|(*:264)'
                .')'
                .'|/vendedores/([^/]++)(?'
                    .'|(*:296)'
                    .'|/edit(*:309)'
                    .'|(*:317)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        24 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        69 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        82 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        101 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        114 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        124 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        159 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        192 => [[['_route' => 'propiedades_show', '_controller' => 'App\\Controller\\PropiedadesController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        205 => [[['_route' => 'propiedades_edit', '_controller' => 'App\\Controller\\PropiedadesController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        213 => [[['_route' => 'propiedades_delete', '_controller' => 'App\\Controller\\PropiedadesController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        243 => [[['_route' => 'usuarios_show', '_controller' => 'App\\Controller\\UsuariosController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        256 => [[['_route' => 'usuarios_edit', '_controller' => 'App\\Controller\\UsuariosController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        264 => [[['_route' => 'usuarios_delete', '_controller' => 'App\\Controller\\UsuariosController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        296 => [[['_route' => 'vendedores_show', '_controller' => 'App\\Controller\\VendedoresController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        309 => [[['_route' => 'vendedores_edit', '_controller' => 'App\\Controller\\VendedoresController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        317 => [
            [['_route' => 'vendedores_delete', '_controller' => 'App\\Controller\\VendedoresController::delete'], ['id'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
