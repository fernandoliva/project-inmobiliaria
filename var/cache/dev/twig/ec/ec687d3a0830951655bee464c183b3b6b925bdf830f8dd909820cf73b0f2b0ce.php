<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* img/icono_estacionamiento.svg */
class __TwigTemplate_1a523ba0fbf24d8c4ae748723afe02cd2406d9ab5b91ce9ff96b702340bb97b2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/icono_estacionamiento.svg"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/icono_estacionamiento.svg"));

        // line 1
        echo "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"40\" height=\"31\" viewBox=\"0 0 40 31\">
  <g id=\"Objeto_inteligente_vectorial\" data-name=\"Objeto inteligente vectorial\" transform=\"translate(-5 -4.539)\">
    <g id=\"Group_17\" data-name=\"Group 17\">
      <g id=\"Group_16\" data-name=\"Group 16\">
        <path id=\"Path_10\" data-name=\"Path 10\" d=\"M43.47,16.057l-.872-.925-3.06-8.264A3.589,3.589,0,0,0,36.2,4.539H14.342A3.577,3.577,0,0,0,11,6.868L7.983,15.029,6.727,16.194A4.924,4.924,0,0,0,5,19.936v12.04a3.576,3.576,0,0,0,3.573,3.562h2.256A3.576,3.576,0,0,0,14.4,31.976V30.229H35.6v1.747a3.576,3.576,0,0,0,3.573,3.562h2.256A3.576,3.576,0,0,0,45,31.976V19.619A4.97,4.97,0,0,0,43.47,16.057ZM12.607,7.45a1.849,1.849,0,0,1,1.735-1.2H36.205a1.861,1.861,0,0,1,1.735,1.2l2.667,7.193H38.615a5.861,5.861,0,0,0-11.6,0H9.94Zm24.265,7.193H28.744a4.15,4.15,0,0,1,8.128,0ZM12.692,31.976a1.86,1.86,0,0,1-1.863,1.85H8.573a1.86,1.86,0,0,1-1.863-1.85V29.733a3.466,3.466,0,0,0,1.863.5h4.12Zm30.6,0a1.86,1.86,0,0,1-1.863,1.85H39.171a1.866,1.866,0,0,1-1.863-1.85V30.229h4.12a3.466,3.466,0,0,0,1.863-.5v2.244Zm-1.863-3.46H8.573a1.846,1.846,0,0,1-1.863-1.833V19.936a3.218,3.218,0,0,1,1.137-2.449l.034-.034,1.162-1.1H41.41l.82.882c.009.017.034.026.043.043a3.23,3.23,0,0,1,1.009,2.338v7.065h.008A1.851,1.851,0,0,1,41.427,28.517Z\"/>
      </g>
    </g>
    <g id=\"Group_19\" data-name=\"Group 19\">
      <g id=\"Group_18\" data-name=\"Group 18\">
        <path id=\"Path_11\" data-name=\"Path 11\" d=\"M16.282,19.782H9.872a.858.858,0,0,0-.855.856v4.453a.858.858,0,0,0,.855.856h6.41a.858.858,0,0,0,.855-.856V20.638A.858.858,0,0,0,16.282,19.782Zm-.855,4.453h-4.7v-2.74h4.7Z\"/>
      </g>
    </g>
    <g id=\"Group_21\" data-name=\"Group 21\">
      <g id=\"Group_20\" data-name=\"Group 20\">
        <path id=\"Path_12\" data-name=\"Path 12\" d=\"M40.128,19.782h-6.41a.858.858,0,0,0-.855.856v4.453a.858.858,0,0,0,.855.856h6.41a.858.858,0,0,0,.855-.856V20.638A.858.858,0,0,0,40.128,19.782Zm-.855,4.453h-4.7v-2.74h4.7Z\"/>
      </g>
    </g>
    <g id=\"Group_23\" data-name=\"Group 23\">
      <g id=\"Group_22\" data-name=\"Group 22\">
        <path id=\"Path_13\" data-name=\"Path 13\" d=\"M29.128,23.378H20.872a.856.856,0,0,0,0,1.713h8.256a.856.856,0,0,0,0-1.713Z\"/>
      </g>
    </g>
    <g id=\"Group_25\" data-name=\"Group 25\">
      <g id=\"Group_24\" data-name=\"Group 24\">
        <path id=\"Path_14\" data-name=\"Path 14\" d=\"M29.128,20.724H20.872a.856.856,0,0,0,0,1.713h8.256a.856.856,0,0,0,0-1.713Z\"/>
      </g>
    </g>
  </g>
</svg>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "img/icono_estacionamiento.svg";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"40\" height=\"31\" viewBox=\"0 0 40 31\">
  <g id=\"Objeto_inteligente_vectorial\" data-name=\"Objeto inteligente vectorial\" transform=\"translate(-5 -4.539)\">
    <g id=\"Group_17\" data-name=\"Group 17\">
      <g id=\"Group_16\" data-name=\"Group 16\">
        <path id=\"Path_10\" data-name=\"Path 10\" d=\"M43.47,16.057l-.872-.925-3.06-8.264A3.589,3.589,0,0,0,36.2,4.539H14.342A3.577,3.577,0,0,0,11,6.868L7.983,15.029,6.727,16.194A4.924,4.924,0,0,0,5,19.936v12.04a3.576,3.576,0,0,0,3.573,3.562h2.256A3.576,3.576,0,0,0,14.4,31.976V30.229H35.6v1.747a3.576,3.576,0,0,0,3.573,3.562h2.256A3.576,3.576,0,0,0,45,31.976V19.619A4.97,4.97,0,0,0,43.47,16.057ZM12.607,7.45a1.849,1.849,0,0,1,1.735-1.2H36.205a1.861,1.861,0,0,1,1.735,1.2l2.667,7.193H38.615a5.861,5.861,0,0,0-11.6,0H9.94Zm24.265,7.193H28.744a4.15,4.15,0,0,1,8.128,0ZM12.692,31.976a1.86,1.86,0,0,1-1.863,1.85H8.573a1.86,1.86,0,0,1-1.863-1.85V29.733a3.466,3.466,0,0,0,1.863.5h4.12Zm30.6,0a1.86,1.86,0,0,1-1.863,1.85H39.171a1.866,1.866,0,0,1-1.863-1.85V30.229h4.12a3.466,3.466,0,0,0,1.863-.5v2.244Zm-1.863-3.46H8.573a1.846,1.846,0,0,1-1.863-1.833V19.936a3.218,3.218,0,0,1,1.137-2.449l.034-.034,1.162-1.1H41.41l.82.882c.009.017.034.026.043.043a3.23,3.23,0,0,1,1.009,2.338v7.065h.008A1.851,1.851,0,0,1,41.427,28.517Z\"/>
      </g>
    </g>
    <g id=\"Group_19\" data-name=\"Group 19\">
      <g id=\"Group_18\" data-name=\"Group 18\">
        <path id=\"Path_11\" data-name=\"Path 11\" d=\"M16.282,19.782H9.872a.858.858,0,0,0-.855.856v4.453a.858.858,0,0,0,.855.856h6.41a.858.858,0,0,0,.855-.856V20.638A.858.858,0,0,0,16.282,19.782Zm-.855,4.453h-4.7v-2.74h4.7Z\"/>
      </g>
    </g>
    <g id=\"Group_21\" data-name=\"Group 21\">
      <g id=\"Group_20\" data-name=\"Group 20\">
        <path id=\"Path_12\" data-name=\"Path 12\" d=\"M40.128,19.782h-6.41a.858.858,0,0,0-.855.856v4.453a.858.858,0,0,0,.855.856h6.41a.858.858,0,0,0,.855-.856V20.638A.858.858,0,0,0,40.128,19.782Zm-.855,4.453h-4.7v-2.74h4.7Z\"/>
      </g>
    </g>
    <g id=\"Group_23\" data-name=\"Group 23\">
      <g id=\"Group_22\" data-name=\"Group 22\">
        <path id=\"Path_13\" data-name=\"Path 13\" d=\"M29.128,23.378H20.872a.856.856,0,0,0,0,1.713h8.256a.856.856,0,0,0,0-1.713Z\"/>
      </g>
    </g>
    <g id=\"Group_25\" data-name=\"Group 25\">
      <g id=\"Group_24\" data-name=\"Group 24\">
        <path id=\"Path_14\" data-name=\"Path 14\" d=\"M29.128,20.724H20.872a.856.856,0,0,0,0,1.713h8.256a.856.856,0,0,0,0-1.713Z\"/>
      </g>
    </g>
  </g>
</svg>
", "img/icono_estacionamiento.svg", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\img\\icono_estacionamiento.svg");
    }
}
