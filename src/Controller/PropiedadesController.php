<?php

namespace App\Controller;

use App\Entity\Propiedades;
use App\Form\PropiedadesType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/propiedades')]
class PropiedadesController extends AbstractController
{
    #[Route('/', name: 'propiedades_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $propiedades = $entityManager
            ->getRepository(Propiedades::class)
            ->findAll();

        return $this->render('propiedades/index.html.twig', [
            'propiedades' => $propiedades,
        ]);
    }

    #[Route('/new', name: 'propiedades_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, SluggerInterface $slugger): Response
    {
        $propiedade = new Propiedades();
        $form = $this->createForm(PropiedadesType::class, $propiedade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($propiedade);

            //Configuracion subida de imagen en local
            $image = $form->get('imagen')->getData();
            $path = $this->getParameter('kernel.project_dir').'/public/build/img/';
            $filename = $slugger->slug($propiedade->getNombre()).'.'.$image->guessExtension();
            $image->move($path, $filename);

            $propiedade->setImagen($filename);
            $entityManager->flush();


            return $this->redirectToRoute('propiedades_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('propiedades/new.html.twig', [
            'propiedade' => $propiedade,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'propiedades_show', methods: ['GET'])]
    public function show(Propiedades $propiedade): Response
    {
        return $this->render('propiedades/show.html.twig', [
            'propiedade' => $propiedade,
        ]);
    }

    #[Route('/{id}/edit', name: 'propiedades_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Propiedades $propiedade, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PropiedadesType::class, $propiedade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('propiedades_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('propiedades/edit.html.twig', [
            'propiedade' => $propiedade,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'propiedades_delete', methods: ['POST'])]
    public function delete(Request $request, Propiedades $propiedade, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$propiedade->getId(), $request->request->get('_token'))) {
            $entityManager->remove($propiedade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('propiedades_index', [], Response::HTTP_SEE_OTHER);
    }
}
