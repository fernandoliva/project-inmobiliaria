<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* img/icono1.svg */
class __TwigTemplate_33a71ed8444453b3e12ffbf13379bc39588f446f50e51d3d58de9e18ccc8f277 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/icono1.svg"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/icono1.svg"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
\t viewBox=\"0 0 123 93\" style=\"enable-background:new 0 0 123 93;\" xml:space=\"preserve\">
<style type=\"text/css\">
\t.st0{clip-path:url(#SVGID_2_);}
\t.st1{fill:#E08709;}
</style>
<g>
\t<defs>
\t\t<rect id=\"SVGID_1_\" width=\"123\" height=\"93\"/>
\t</defs>
\t<clipPath id=\"SVGID_2_\">
\t\t<use xlink:href=\"#SVGID_1_\"  style=\"overflow:visible;\"/>
\t</clipPath>
\t<g id=\"Watch_38mm_1\" class=\"st0\">
\t\t<g id=\"icono1\" transform=\"translate(-1.153)\">
\t\t\t<g id=\"Group_1\" transform=\"translate(24.153)\">
\t\t\t\t<path id=\"Path_1\" class=\"st1\" d=\"M56.1,37.5V18.9C56.1,8.5,46.8,0,35.4,0S14.8,8.5,14.8,18.9v18.6H0V93h72.5V37.5H56.1z
\t\t\t\t\t M18.1,18.9c0-8.6,7.8-15.7,17.3-15.7s17.3,7,17.3,15.7v18.6H18.1L18.1,18.9z M69.2,89.7H3.3v-6.5h65.9V89.7z M3.3,79.9V40.8
\t\t\t\t\th65.9v39.2H3.3z\"/>
\t\t\t\t<path id=\"Path_2\" class=\"st1\" d=\"M36.3,50.6c-3.6,0-6.6,2.9-6.6,6.5v9.8c0,3.6,2.9,6.6,6.5,6.7c3.6,0,6.6-2.9,6.7-6.5
\t\t\t\t\tc0-0.1,0-0.1,0-0.2v-9.8C42.8,53.5,39.9,50.6,36.3,50.6L36.3,50.6z M39.6,66.9c0,1.8-1.5,3.3-3.3,3.3c-1.8,0-3.3-1.5-3.3-3.3
\t\t\t\t\tc0,0,0,0,0,0v-9.8c0-1.8,1.5-3.3,3.3-3.3c1.8,0,3.3,1.5,3.3,3.3c0,0,0,0,0,0V66.9z\"/>
\t\t\t</g>
\t\t</g>
\t</g>
</g>
</svg>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "img/icono1.svg";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 24.1.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
\t viewBox=\"0 0 123 93\" style=\"enable-background:new 0 0 123 93;\" xml:space=\"preserve\">
<style type=\"text/css\">
\t.st0{clip-path:url(#SVGID_2_);}
\t.st1{fill:#E08709;}
</style>
<g>
\t<defs>
\t\t<rect id=\"SVGID_1_\" width=\"123\" height=\"93\"/>
\t</defs>
\t<clipPath id=\"SVGID_2_\">
\t\t<use xlink:href=\"#SVGID_1_\"  style=\"overflow:visible;\"/>
\t</clipPath>
\t<g id=\"Watch_38mm_1\" class=\"st0\">
\t\t<g id=\"icono1\" transform=\"translate(-1.153)\">
\t\t\t<g id=\"Group_1\" transform=\"translate(24.153)\">
\t\t\t\t<path id=\"Path_1\" class=\"st1\" d=\"M56.1,37.5V18.9C56.1,8.5,46.8,0,35.4,0S14.8,8.5,14.8,18.9v18.6H0V93h72.5V37.5H56.1z
\t\t\t\t\t M18.1,18.9c0-8.6,7.8-15.7,17.3-15.7s17.3,7,17.3,15.7v18.6H18.1L18.1,18.9z M69.2,89.7H3.3v-6.5h65.9V89.7z M3.3,79.9V40.8
\t\t\t\t\th65.9v39.2H3.3z\"/>
\t\t\t\t<path id=\"Path_2\" class=\"st1\" d=\"M36.3,50.6c-3.6,0-6.6,2.9-6.6,6.5v9.8c0,3.6,2.9,6.6,6.5,6.7c3.6,0,6.6-2.9,6.7-6.5
\t\t\t\t\tc0-0.1,0-0.1,0-0.2v-9.8C42.8,53.5,39.9,50.6,36.3,50.6L36.3,50.6z M39.6,66.9c0,1.8-1.5,3.3-3.3,3.3c-1.8,0-3.3-1.5-3.3-3.3
\t\t\t\t\tc0,0,0,0,0,0v-9.8c0-1.8,1.5-3.3,3.3-3.3c1.8,0,3.3,1.5,3.3,3.3c0,0,0,0,0,0V66.9z\"/>
\t\t\t</g>
\t\t</g>
\t</g>
</g>
</svg>
", "img/icono1.svg", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\img\\icono1.svg");
    }
}
