<?php

namespace App\Controller;

use App\Entity\Propiedades;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    /**
    * @Route("/", methods={"GET"}, name="homePage")
    */

    public function index(EntityManagerInterface $entityManager): Response
    {

        $propiedades = $entityManager
            ->getRepository(Propiedades::class)
            ->findAll();

        return $this->render('home/home.html.twig', [
            'propiedades' => $propiedades,
        ]);
    }
}