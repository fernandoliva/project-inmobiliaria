<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* img/icono_wc.svg */
class __TwigTemplate_e1a6111c20f5c82f5ff3348e4f758c9fe5169d007b2603b6d874c7e988a90cf3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/icono_wc.svg"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "img/icono_wc.svg"));

        // line 1
        echo "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"34\" height=\"37\" viewBox=\"0 0 34 37\">
  <g id=\"Objeto_inteligente_vectorial\" data-name=\"Objeto inteligente vectorial\" transform=\"translate(-6.182)\">
    <g id=\"Group_15\" data-name=\"Group 15\">
      <g id=\"Group_14\" data-name=\"Group 14\">
        <path id=\"Path_9\" data-name=\"Path 9\" d=\"M37.091,16.65H21.018V3.587A1.845,1.845,0,0,0,20.4,0H8.036a1.845,1.845,0,0,0-.618,3.587V18.5a1.852,1.852,0,0,0,1.855,1.85h1.855V25.9a5.563,5.563,0,0,0,5.564,5.55v4.933a.617.617,0,0,0,.618.617H28.437a.617.617,0,0,0,.618-.617V31.419a10.5,10.5,0,0,0,9.891-10.452V20.35h.618a.617.617,0,0,0,.618-.617A3.091,3.091,0,0,0,37.091,16.65ZM7.418,1.85a.617.617,0,0,1,.618-.617H20.4a.617.617,0,1,1,0,1.233H8.036A.617.617,0,0,1,7.418,1.85ZM9.273,19.117a.617.617,0,0,1-.618-.617V3.7H19.782V19.117Zm18.546,16.65H17.927V31.45h9.891Zm9.891-14.8a9.272,9.272,0,0,1-9.273,9.25H16.691A4.328,4.328,0,0,1,12.364,25.9V20.35H37.71Zm-16.691-1.85V17.883H37.091a1.858,1.858,0,0,1,1.749,1.233Z\"/>
      </g>
    </g>
  </g>
</svg>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "img/icono_wc.svg";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"34\" height=\"37\" viewBox=\"0 0 34 37\">
  <g id=\"Objeto_inteligente_vectorial\" data-name=\"Objeto inteligente vectorial\" transform=\"translate(-6.182)\">
    <g id=\"Group_15\" data-name=\"Group 15\">
      <g id=\"Group_14\" data-name=\"Group 14\">
        <path id=\"Path_9\" data-name=\"Path 9\" d=\"M37.091,16.65H21.018V3.587A1.845,1.845,0,0,0,20.4,0H8.036a1.845,1.845,0,0,0-.618,3.587V18.5a1.852,1.852,0,0,0,1.855,1.85h1.855V25.9a5.563,5.563,0,0,0,5.564,5.55v4.933a.617.617,0,0,0,.618.617H28.437a.617.617,0,0,0,.618-.617V31.419a10.5,10.5,0,0,0,9.891-10.452V20.35h.618a.617.617,0,0,0,.618-.617A3.091,3.091,0,0,0,37.091,16.65ZM7.418,1.85a.617.617,0,0,1,.618-.617H20.4a.617.617,0,1,1,0,1.233H8.036A.617.617,0,0,1,7.418,1.85ZM9.273,19.117a.617.617,0,0,1-.618-.617V3.7H19.782V19.117Zm18.546,16.65H17.927V31.45h9.891Zm9.891-14.8a9.272,9.272,0,0,1-9.273,9.25H16.691A4.328,4.328,0,0,1,12.364,25.9V20.35H37.71Zm-16.691-1.85V17.883H37.091a1.858,1.858,0,0,1,1.749,1.233Z\"/>
      </g>
    </g>
  </g>
</svg>
", "img/icono_wc.svg", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\img\\icono_wc.svg");
    }
}
