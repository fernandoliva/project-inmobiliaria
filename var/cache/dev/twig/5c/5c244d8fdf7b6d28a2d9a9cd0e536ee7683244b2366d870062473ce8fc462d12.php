<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/home.html.twig */
class __TwigTemplate_c66ed401814cb186e69f81293d26f77238c3e0c364524da7299f8d0b758aa7c2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/home.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <main class=\"contenedor seccion\">
        <h1>Sobre nosotros</h1>
        <div class=\"iconos-nosotros\">
            <div class=\"icono\">
                <img src=\"build/img/icono1.svg\" alt=\"Icono seguridad\" loading=\"lazy\">
                <h3>Seguridad</h3>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class=\"icono\">
                <img src=\"build/img/icono2.svg\" alt=\"Icono precio\" loading=\"lazy\">
                <h3>Precio</h3>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class=\"icono\">
                <img src=\"build/img/icono3.svg\" alt=\"Icono tiempo\" loading=\"lazy\">
                <h3>Tiempo</h3>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
            </div>
        </div>
    </main>
    <section class=\"seccion contenedor\">
        <h2>Casas y Pisos en venta</h2>

        <div class=\"contenedor-anuncios\">
        ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["propiedades"]) || array_key_exists("propiedades", $context) ? $context["propiedades"] : (function () { throw new RuntimeError('Variable "propiedades" does not exist.', 28, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["propiedade"]) {
            // line 29
            echo "            <div class=\"anuncio\">
                <img src=";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("../../build/img/" . twig_get_attribute($this->env, $this->source, $context["propiedade"], "imagen", [], "any", false, false, false, 30))), "html", null, true);
            echo " alt=\"Anuncio\" loading=\"lazy\">
                <div class=\"contenido-anuncio\">
                    <h3>";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "nombre", [], "any", false, false, false, 32), "html", null, true);
            echo "</h3>
                    <p>";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "descripcion", [], "any", false, false, false, 33), "html", null, true);
            echo "</p>
                    <p class=\"precio\">";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "precio", [], "any", false, false, false, 34), "html", null, true);
            echo "€</p>
                    <ul class=\"iconos-caracteristicas\">
                        <li>
                            <img class=\"icono\" src=\"build/img/icono_wc.svg\" alt=\"icono wc\" loading=\"lazy\">
                            <p>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "wc", [], "any", false, false, false, 38), "html", null, true);
            echo "</p>
                        </li>
                        <li>
                            <img class=\"icono\" src=\"build/img/icono_estacionamiento.svg\" alt=\"icono estacionamiento\" loading=\"lazy\">
                            <p>";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "parking", [], "any", false, false, false, 42), "html", null, true);
            echo "</p>
                        </li>
                        <li>
                            <img class=\"icono\" src=\"build/img/icono_dormitorio.svg\" alt=\"icono dormitorio\" loading=\"lazy\">
                            <p>";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["propiedade"], "habitaciones", [], "any", false, false, false, 46), "html", null, true);
            echo "</p>
                        </li>
                    </ul>
                </div> <!--Contenido anuncio-->
            </div><!--Anuncio-->
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['propiedade'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "        </div><!--Contenedor de anuncios-->
    </section>
    <section class=\"imagen-contacto\" style=\"background-image:url('";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/build/img/encuentra.jpg"), "html", null, true);
        echo "')\">
        <h2>Encuentra la casa de tus sueños</h2>
        <p>Llena el formulario de contacto y un asesor se pondrá en contacto contigo</p>
        <a href=\"/contact\" class=\"boton boton-amarillo\">Contáctanos</a>
    </section>
    <div class=\"contenedor seccion seccion-inferior\">
        <section class=\"blog\">
            <h3>Nuestro Blog</h3>
            <article class=\"entrada-blog\">
                <div class=\"imagen\">
                    <picture>
                        <source srcset=\"/build/img/blog1.webp\" type=\"image/webp\">
                        <source srcset=\"/build/img/blog1.jpg\" type=\"image/jpg\">
                        <img src=\"/build/img/blog1.jpg\" alt=\"Texto entrada blog\" loading=\"lazy\">
                    </picture>
                </div>
                <div class=\"texto-entrada\">
                    <a href=\"#\">
                        <h4>Terraza en el techo de tu casa</h4>
                        <p class=\"info-meta\">Escrito el: <span>20/10/2021</span> por: <span>Admin</span></p>
                        <p>Consejos para construir una terraza en el techo de tu casa con los mejores materiales y ahorrando dinero.</p>
                    </a>
                </div>
            </article>
            <article class=\"entrada-blog\">
                <div class=\"imagen\">
                    <picture>
                        <source srcset=\"build/img/blog2.webp\" type=\"image/webp\">
                        <source srcset=\"build/img/blog2.jpg\" type=\"image/jpg\">
                        <img src=\"build/img/blog2.jpg\" alt=\"Texto entrada blog\" loading=\"lazy\">
                    </picture>
                </div>
                <div class=\"texto-entrada\">
                    <a href=\"#\">
                        <h4>Guia para la decoración de tu hogar</h4>
                        <p class=\"info-meta\">Escrito el: <span>15/10/2021</span> por: <span>Jaime</span></p>
                        <p>Maximiza el espacio en tu hogar con esta guia, aprende a combinar muebles y colores para darle vida a tu espacio</p>
                    </a>
                </div>
            </article>
        </section>
        <section class=\"testimonials\">
            <h3>Testimonials</h3>
            <div class=\"testimonial\">
                <blockquote>
                    El personal se ha comportado de una excelente forma, muy buena atencion y la casa que me ofrecieron cumple con todas mis expectativas.
                </blockquote>
                <p>- Juan de la Torre</p>
            </div>
        </section>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 54,  147 => 52,  135 => 46,  128 => 42,  121 => 38,  114 => 34,  110 => 33,  106 => 32,  101 => 30,  98 => 29,  94 => 28,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block body %}
    <main class=\"contenedor seccion\">
        <h1>Sobre nosotros</h1>
        <div class=\"iconos-nosotros\">
            <div class=\"icono\">
                <img src=\"build/img/icono1.svg\" alt=\"Icono seguridad\" loading=\"lazy\">
                <h3>Seguridad</h3>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class=\"icono\">
                <img src=\"build/img/icono2.svg\" alt=\"Icono precio\" loading=\"lazy\">
                <h3>Precio</h3>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
            </div>
            <div class=\"icono\">
                <img src=\"build/img/icono3.svg\" alt=\"Icono tiempo\" loading=\"lazy\">
                <h3>Tiempo</h3>
                <p>Lorem ipsum dolor sit amet, consectetur </p>
            </div>
        </div>
    </main>
    <section class=\"seccion contenedor\">
        <h2>Casas y Pisos en venta</h2>

        <div class=\"contenedor-anuncios\">
        {% for propiedade in propiedades %}
            <div class=\"anuncio\">
                <img src={{ asset('../../build/img/' ~ propiedade.imagen) }} alt=\"Anuncio\" loading=\"lazy\">
                <div class=\"contenido-anuncio\">
                    <h3>{{ propiedade.nombre }}</h3>
                    <p>{{ propiedade.descripcion }}</p>
                    <p class=\"precio\">{{ propiedade.precio }}€</p>
                    <ul class=\"iconos-caracteristicas\">
                        <li>
                            <img class=\"icono\" src=\"build/img/icono_wc.svg\" alt=\"icono wc\" loading=\"lazy\">
                            <p>{{ propiedade.wc }}</p>
                        </li>
                        <li>
                            <img class=\"icono\" src=\"build/img/icono_estacionamiento.svg\" alt=\"icono estacionamiento\" loading=\"lazy\">
                            <p>{{ propiedade.parking }}</p>
                        </li>
                        <li>
                            <img class=\"icono\" src=\"build/img/icono_dormitorio.svg\" alt=\"icono dormitorio\" loading=\"lazy\">
                            <p>{{ propiedade.habitaciones }}</p>
                        </li>
                    </ul>
                </div> <!--Contenido anuncio-->
            </div><!--Anuncio-->
        {% endfor %}
        </div><!--Contenedor de anuncios-->
    </section>
    <section class=\"imagen-contacto\" style=\"background-image:url('{{asset('/build/img/encuentra.jpg')}}')\">
        <h2>Encuentra la casa de tus sueños</h2>
        <p>Llena el formulario de contacto y un asesor se pondrá en contacto contigo</p>
        <a href=\"/contact\" class=\"boton boton-amarillo\">Contáctanos</a>
    </section>
    <div class=\"contenedor seccion seccion-inferior\">
        <section class=\"blog\">
            <h3>Nuestro Blog</h3>
            <article class=\"entrada-blog\">
                <div class=\"imagen\">
                    <picture>
                        <source srcset=\"/build/img/blog1.webp\" type=\"image/webp\">
                        <source srcset=\"/build/img/blog1.jpg\" type=\"image/jpg\">
                        <img src=\"/build/img/blog1.jpg\" alt=\"Texto entrada blog\" loading=\"lazy\">
                    </picture>
                </div>
                <div class=\"texto-entrada\">
                    <a href=\"#\">
                        <h4>Terraza en el techo de tu casa</h4>
                        <p class=\"info-meta\">Escrito el: <span>20/10/2021</span> por: <span>Admin</span></p>
                        <p>Consejos para construir una terraza en el techo de tu casa con los mejores materiales y ahorrando dinero.</p>
                    </a>
                </div>
            </article>
            <article class=\"entrada-blog\">
                <div class=\"imagen\">
                    <picture>
                        <source srcset=\"build/img/blog2.webp\" type=\"image/webp\">
                        <source srcset=\"build/img/blog2.jpg\" type=\"image/jpg\">
                        <img src=\"build/img/blog2.jpg\" alt=\"Texto entrada blog\" loading=\"lazy\">
                    </picture>
                </div>
                <div class=\"texto-entrada\">
                    <a href=\"#\">
                        <h4>Guia para la decoración de tu hogar</h4>
                        <p class=\"info-meta\">Escrito el: <span>15/10/2021</span> por: <span>Jaime</span></p>
                        <p>Maximiza el espacio en tu hogar con esta guia, aprende a combinar muebles y colores para darle vida a tu espacio</p>
                    </a>
                </div>
            </article>
        </section>
        <section class=\"testimonials\">
            <h3>Testimonials</h3>
            <div class=\"testimonial\">
                <blockquote>
                    El personal se ha comportado de una excelente forma, muy buena atencion y la casa que me ofrecieron cumple con todas mis expectativas.
                </blockquote>
                <p>- Juan de la Torre</p>
            </div>
        </section>
    </div>
{% endblock %}", "home/home.html.twig", "C:\\Users\\Nando\\Desktop\\upgrade\\php\\symfony\\projectInmo\\templates\\home\\home.html.twig");
    }
}
